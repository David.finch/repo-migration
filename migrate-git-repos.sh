#!/bin/bash

# Usage:
#  Will mirror an existing Git Repo and push it to a fresh git repository
#  Note: if repository exists and has protected branchs the push will be rejected

set -o xtrace
FROMREPO="$1"
TOREPO="$2"

basename=$(basename $FROMREPO)
echo $basename
git clone --mirror $FROMREPO
cd $basename
git remote set-url origin $TOREPO
git push --mirror -f
rm -rf ../$basename
